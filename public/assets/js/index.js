addcarts = document.querySelectorAll('.add-cart');

products = [
	{
		name: 'Vans Old Skool Core Classic',
		tag: 'shoes1',
		price: 1299.99,
		inCart:0
	},
	{
		name: 'Vans Old Skool: Black',
		tag: 'shoes2',
		price: 1299.99,
		inCart:0
	},
	{
		name: 'Decor Bracelet',
		tag: 'accessories1',
		price: 299.99,
		inCart:0
	},
	{
		name: 'Jumper in Camel',
		tag: 'women1',
		price: 599.99,
		inCart:0
	},
	{
		name: 'Cut And Sew Panel Crop Sweatshirt',
		tag: 'women2',
		price: 699.99,
		inCart:0
	},
	{
		name: 'Figure Graphic Pullover',
		tag: 'men1',
		price: 899.99,
		inCart:0
	},
	{
		name: 'Fire Print Pullover',
		tag: 'men2',
		price: 899.99,
		inCart:0
	},
	{
		name: 'Frada Velvet Bag',
		tag: 'bag1',
		price: 1599.99,
		inCart:0
	},
	{
		name: 'Double Handle Satchel Bag',
		tag: 'bag2',
		price: 1799.99,
		inCart:0
	},
	{
		name: 'Copic Original doubleended marker',
		tag: 'art1',
		price: 499.99,
		inCart:0
	}
]



for (let i=0; i < addcarts.length; i++){
	addcarts[i].addEventListener('click', () =>{
		cartNumbers(products[i]);
		totalCost(products[i]);
	})
}

function onLoadCartNumbers(){
	productNumbers = localStorage.getItem('cartNumbers');

	if(productNumbers){
		document.querySelector('.cart span').innerHTML= productNumbers;
	}
}

function cartNumbers(product){
	console.log("the product clicked is", product);
	productNumbers = localStorage.getItem('cartNumbers');
	productNumbers = parseInt(productNumbers);
	
	if( productNumbers ){
		localStorage.setItem('cartNumbers', productNumbers + 1);
		document.querySelector('.cart span').innerHTML= productNumbers + 1;
	}else{
		localStorage.setItem('cartNumbers', 1);
		document.querySelector('.cart span').innerHTML= 1;
	}

	setItems(product);
}

function setItems(product){
	cartItems = localStorage.getItem('productsInCart');
	cartItems = JSON.parse(cartItems);
	
	if(cartItems != null){
		if(cartItems[product.tag] == undefined){
			cartItems ={
				...cartItems,
				[product.tag]:product
			}
		}
		cartItems[product.tag].inCart += 1;
	}else{
		product.inCart = 1;
		cartItems = {
			[product.tag]:product
		}
	}
	
	localStorage.setItem("productsInCart", JSON.stringify(cartItems));
}


function totalCost(product){
	cartCost = localStorage.getItem('totalCost');
	
	console.log("My cartCost is", cartCost);
	console.log(typeof cartCost);

	if(cartCost !=null){
		cartCost =parseFloat(cartCost);
		localStorage.setItem("totalCost", cartCost + product.price);
	}else{
		localStorage.setItem("totalCost", product.price);
	}
	
}

function displayCart(){
	cartItems = localStorage.getItem('productsInCart');
	cartItems = JSON.parse(cartItems);
	productContainer = document.querySelector('.products');

	console.log(cartItems);
	if(cartItems && productContainer){
		productContainer.innerHTML = '';
		Object.values(cartItems).map(item => {
			productContainer.innerHTML += `
			<div class = "product"
				<ion-icon name="trash-outline"></ion-icon>
				<img src="../assets/images/${item.tag}.jpg">
				<span>${item.name}</span>
			</div>
			<div class ="price">${item.price}</div>
			<div class ="quantity">
			<ion-icon class"minus" name="remove-circle-outline"></ion-icon>
			<span>${item.inCart}</span>
			<ion-icon class="plus" name="add-circle-outline"></ion-icon>
			</div>
			<div class="total">
				Php${item.inCart * item.price}
			</div>
			`;
		});

		productContainer.innerHTML +=`
		<div class="basketTotalContainer">
			<h4 class="basketTotalTitle">
				Total
			</h4>
			<h4 class="totalCart">
				Php${cartCost}
			</h4>
		`;
	}
}

onLoadCartNumbers();
displayCart();
